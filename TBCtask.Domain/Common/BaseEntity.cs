﻿using System.ComponentModel.DataAnnotations;

namespace TBCtask.Domain.Common
{
    public abstract class BaseEntity
    {
        [Key]
        public long Id { get; set; }

        public DateTime Created { get; set; } = DateTime.UtcNow;

        public DateTime? LastModified { get; set; } = DateTime.UtcNow;

        public bool Deleted { get; set; }

        public byte[] RowVersion { get; set; }
    }
}
