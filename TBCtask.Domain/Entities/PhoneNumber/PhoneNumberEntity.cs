﻿using TBCtask.Domain.Common;
using TBCtask.Domain.Entities.Person;
using TBCtask.Domain.Enums;

namespace TBCtask.Domain.Entities.PhoneNumber
{
    public class PhoneNumberEntity : BaseEntity
    {
        public PhoneNumberType NumberType { get; set; }
        public string Number { get; set; }

        public long PersonId { get; set; }

        public long RelatedPersonId { get; set; }

        public PersonEntity Person { get; set; }

    }
}
