﻿using TBCtask.Domain.Common;
using TBCtask.Domain.Entities.Person;

namespace TBCtask.Domain.Entities.City
{
    public class CityEntity : BaseEntity
    {
        public string Name { get; set; }
        public ICollection<PersonEntity> People { get; set; } = new List<PersonEntity>();
    }
}
