﻿using TBCtask.Domain.Common;
using TBCtask.Domain.Enums;

namespace TBCtask.Domain.Entities.Person
{
    public class RelatedPersonEntity : BaseEntity
    {
        public long PersonId { get; set; }
        public long RelatedPersonId { get; set; }
        public PersonConnectionType RelationshipType { get; set; }
        public PersonEntity Person { get; set; }
    }
}
