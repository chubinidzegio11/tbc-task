﻿using System.ComponentModel.DataAnnotations;
using TBCtask.Domain.Common;
using TBCtask.Domain.Entities.City;
using TBCtask.Domain.Entities.PhoneNumber;
using TBCtask.Domain.Enums;

namespace TBCtask.Domain.Entities.Person
{
    public class PersonEntity : BaseEntity
    {
        [StringLength(50, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 2)]
        public string FirstName { get; set; }

        [StringLength(50, ErrorMessage = "{0} length must be between {2} and {1}.", MinimumLength = 2)]
        public string LastName { get; set; }
        public GenderType Gender { get; set; }

        [StringLength(11, ErrorMessage = "{0} length must be between {11}.", MinimumLength = 11)]
        public string IdentificationNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string? Image { get; set; }
        public long CityId { get; set; }
        public CityEntity City { get; set; }
        public ICollection<RelatedPersonEntity>? RelatedPersons { get; set; } = new List<RelatedPersonEntity>();
        public ICollection<PhoneNumberEntity>? PhoneNumbers { get; set; } = new List<PhoneNumberEntity>();
    }
}
