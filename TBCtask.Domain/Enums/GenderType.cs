﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBCtask.Domain.Enums
{
    public enum GenderType
    {
        Male = 1,
        Female = 2,
    }
}
