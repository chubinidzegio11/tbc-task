﻿namespace TBCtask.Domain.Enums
{
    public enum PhoneNumberType
    {
        Mobile = 1,
        Home = 2,
        Office = 3
    }
}
