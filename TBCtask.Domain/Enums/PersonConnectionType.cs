﻿namespace TBCtask.Domain.Enums
{
    public enum PersonConnectionType
    {
        Relative,
        Friend,
        Colleague,
        Acquaintance,
        Other
    }
}
