﻿using Azure.Core.Serialization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using System.Net;
using System.Text.Json;
using TBCtask.Application.Common.Exceptions;
using TBCtask.Application.Common.Helper;

namespace TBCtask.Infrastructure.Middleware
{
    public class ErrorHandlingMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlingMiddleware> _logger;

        public ErrorHandlingMiddleware(RequestDelegate next, ILogger<ErrorHandlingMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                await HandleExceptionAsync(context, ex);
            }
        }

        private Task HandleExceptionAsync(HttpContext context, Exception exception)
        {
            var result = new ServiceResult<object>();
            var statusCode = HttpStatusCode.InternalServerError;

            switch (exception)
            {
                case DomainException ex:
                    statusCode = HttpStatusCode.BadRequest;
                    result.Message = exception.Message;
                    _logger.LogError(exception, $"{exception.Message}");
                    break;
                case NotFoundException ex:
                    statusCode = HttpStatusCode.NotFound;
                    result.Message = exception.Message;
                    _logger.LogError(exception, $"{exception.Message}");

                    break;
                default:
                    statusCode = HttpStatusCode.InternalServerError;
                    result.Message = $"Something went wrong {context.TraceIdentifier}";
                    _logger.LogError(exception, "Unhandled error occurred");
                    break;
            }

            context.Response.ContentType = "microservice/json";
            context.Response.StatusCode = (int)statusCode;

            return context.Response.WriteAsync(JsonSerializer.Serialize(result));
        }
    }
}
