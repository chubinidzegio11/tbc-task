﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace TBCtask.Infrastructure.Migrations
{
    /// <inheritdoc />
    public partial class init : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Cities",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModified = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    RowVersion = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Cities", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Persons",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    LastName = table.Column<string>(type: "nvarchar(50)", maxLength: 50, nullable: false),
                    Gender = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    IdentificationNumber = table.Column<string>(type: "nvarchar(11)", maxLength: 11, nullable: false),
                    DateOfBirth = table.Column<DateTime>(type: "datetime2", nullable: false),
                    Image = table.Column<string>(type: "nvarchar(max)", nullable: true),
                    CityId = table.Column<long>(type: "bigint", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModified = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    RowVersion = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Persons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Persons_Cities_CityId",
                        column: x => x.CityId,
                        principalTable: "Cities",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PhoneNumbers",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    NumberType = table.Column<string>(type: "nvarchar(max)", nullable: false),
                    Number = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    PersonId = table.Column<long>(type: "bigint", nullable: false),
                    RelatedPersonId = table.Column<long>(type: "bigint", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModified = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    RowVersion = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PhoneNumbers", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PhoneNumbers_Persons_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "RelatedPersons",
                columns: table => new
                {
                    Id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    PersonId = table.Column<long>(type: "bigint", nullable: false),
                    RelatedPersonId = table.Column<long>(type: "bigint", nullable: false),
                    RelationshipType = table.Column<string>(type: "nvarchar(450)", nullable: false),
                    Created = table.Column<DateTime>(type: "datetime2", nullable: false),
                    LastModified = table.Column<DateTime>(type: "datetime2", nullable: true),
                    Deleted = table.Column<bool>(type: "bit", nullable: false),
                    RowVersion = table.Column<byte[]>(type: "rowversion", rowVersion: true, nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RelatedPersons", x => x.Id);
                    table.ForeignKey(
                        name: "FK_RelatedPersons_Persons_PersonId",
                        column: x => x.PersonId,
                        principalTable: "Persons",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.InsertData(
                table: "Cities",
                columns: new[] { "Id", "Created", "Deleted", "LastModified", "Name" },
                values: new object[,]
                {
                    { 1L, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5986), false, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5982), "New York" },
                    { 2L, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5988), false, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5987), "Los Angeles" },
                    { 3L, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5989), false, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5989), "Chicago" },
                    { 4L, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5991), false, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5990), "Houston" },
                    { 5L, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5992), false, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5992), "Phoenix" },
                    { 6L, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5994), false, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5993), "Philadelphia" },
                    { 7L, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5995), false, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5994), "San Antonio" },
                    { 8L, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5996), false, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5996), "San Diego" },
                    { 9L, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5998), false, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5997), "Dallas" },
                    { 10L, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5999), false, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(5999), "San Jose" }
                });

            migrationBuilder.InsertData(
                table: "Persons",
                columns: new[] { "Id", "CityId", "Created", "DateOfBirth", "Deleted", "FirstName", "Gender", "IdentificationNumber", "Image", "LastModified", "LastName" },
                values: new object[,]
                {
                    { 1L, 1L, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(8758), new DateTime(1980, 1, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "John", "Male", "12345678901", "john_doe.jpg", new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(8754), "Doe" },
                    { 2L, 2L, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(8762), new DateTime(1985, 5, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "Alice", "Female", "98765432109", "alice_smith.jpg", new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(8760), "Smith" },
                    { 3L, 2L, new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(8764), new DateTime(1985, 5, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), false, "Alice", "Female", "98765432109", "alice_smith.jpg", new DateTime(2024, 3, 28, 13, 47, 33, 63, DateTimeKind.Utc).AddTicks(8763), "Smith" }
                });

            migrationBuilder.InsertData(
                table: "PhoneNumbers",
                columns: new[] { "Id", "Created", "Deleted", "LastModified", "Number", "NumberType", "PersonId", "RelatedPersonId" },
                values: new object[,]
                {
                    { 1L, new DateTime(2024, 3, 28, 13, 47, 33, 64, DateTimeKind.Utc).AddTicks(1224), false, new DateTime(2024, 3, 28, 13, 47, 33, 64, DateTimeKind.Utc).AddTicks(1222), "1234567890", "Mobile", 1L, 0L },
                    { 2L, new DateTime(2024, 3, 28, 13, 47, 33, 64, DateTimeKind.Utc).AddTicks(1227), false, new DateTime(2024, 3, 28, 13, 47, 33, 64, DateTimeKind.Utc).AddTicks(1226), "9876543210", "Home", 2L, 0L },
                    { 3L, new DateTime(2024, 3, 28, 13, 47, 33, 64, DateTimeKind.Utc).AddTicks(1228), false, new DateTime(2024, 3, 28, 13, 47, 33, 64, DateTimeKind.Utc).AddTicks(1228), "5555555555", "Mobile", 3L, 0L }
                });

            migrationBuilder.InsertData(
                table: "RelatedPersons",
                columns: new[] { "Id", "Created", "Deleted", "LastModified", "PersonId", "RelatedPersonId", "RelationshipType" },
                values: new object[,]
                {
                    { 1L, new DateTime(2024, 3, 28, 13, 47, 33, 64, DateTimeKind.Utc).AddTicks(3880), false, new DateTime(2024, 3, 28, 13, 47, 33, 64, DateTimeKind.Utc).AddTicks(3878), 1L, 2L, "Relative" },
                    { 2L, new DateTime(2024, 3, 28, 13, 47, 33, 64, DateTimeKind.Utc).AddTicks(3882), false, new DateTime(2024, 3, 28, 13, 47, 33, 64, DateTimeKind.Utc).AddTicks(3881), 2L, 3L, "Friend" }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Cities_Id",
                table: "Cities",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Cities_Name",
                table: "Cities",
                column: "Name");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_CityId",
                table: "Persons",
                column: "CityId");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_FirstName",
                table: "Persons",
                column: "FirstName");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_Id",
                table: "Persons",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_Persons_IdentificationNumber",
                table: "Persons",
                column: "IdentificationNumber");

            migrationBuilder.CreateIndex(
                name: "IX_Persons_LastName",
                table: "Persons",
                column: "LastName");

            migrationBuilder.CreateIndex(
                name: "IX_PhoneNumbers_Id",
                table: "PhoneNumbers",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_PhoneNumbers_Number",
                table: "PhoneNumbers",
                column: "Number");

            migrationBuilder.CreateIndex(
                name: "IX_PhoneNumbers_PersonId",
                table: "PhoneNumbers",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_RelatedPersons_Id",
                table: "RelatedPersons",
                column: "Id",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_RelatedPersons_PersonId",
                table: "RelatedPersons",
                column: "PersonId");

            migrationBuilder.CreateIndex(
                name: "IX_RelatedPersons_RelatedPersonId",
                table: "RelatedPersons",
                column: "RelatedPersonId");

            migrationBuilder.CreateIndex(
                name: "IX_RelatedPersons_RelationshipType",
                table: "RelatedPersons",
                column: "RelationshipType");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "PhoneNumbers");

            migrationBuilder.DropTable(
                name: "RelatedPersons");

            migrationBuilder.DropTable(
                name: "Persons");

            migrationBuilder.DropTable(
                name: "Cities");
        }
    }
}
