﻿using Microsoft.EntityFrameworkCore;
using System.Reflection;
using TBCtask.Domain.Entities.City;
using TBCtask.Domain.Entities.Person;
using TBCtask.Domain.Entities.PhoneNumber;

namespace TBCtask.Infrastructure.Persistance
{
    public class AppDbContext : DbContext
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<PersonEntity> Persons { get; set; }

        public DbSet<CityEntity> Cities { get; set;}

        public DbSet<RelatedPersonEntity> RelatedPersons { get; set; }

        public DbSet<PhoneNumberEntity> PhoneNumbers { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            builder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            base.OnModelCreating(builder);
        }
    }
}
