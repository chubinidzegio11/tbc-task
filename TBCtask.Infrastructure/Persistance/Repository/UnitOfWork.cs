﻿using TBCtask.Application.Common.Interfaces;

namespace TBCtask.Infrastructure.Persistance.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly AppDbContext _context;

        public UnitOfWork(AppDbContext context)
        {
            _context = context;
        }

        public async Task TryCommitAsync(Action action)
        {
            await using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                action();
                await transaction.CommitAsync();
            }
            catch (Exception)
            {

               await transaction.RollbackAsync();
                throw;
            }
        }

        public async Task TryCommitAsync(Func<Task> action)
        {
            await using var transaction = await _context.Database.BeginTransactionAsync();
            try
            {
                await action();
                await transaction.CommitAsync();
            }
            catch (Exception)
            {

                await transaction.RollbackAsync();
                throw;
            }
        }
    }
}
