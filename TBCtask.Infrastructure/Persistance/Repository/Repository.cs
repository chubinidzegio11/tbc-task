﻿using Microsoft.EntityFrameworkCore;
using System.Linq.Expressions;
using TBCtask.Application.Common.Interfaces;
using TBCtask.Domain.Common;

namespace TBCtask.Infrastructure.Persistance.Repository
{
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly AppDbContext _context;

        public Repository(AppDbContext context)
        {
            _context = context;
        }

        public async Task<TEntity> GetByIdAsync(long id)
        {
            return await _context.Set<TEntity>().FindAsync(id);
        }

        public async Task<IEnumerable<TEntity>> GetByIdsAsync(IEnumerable<long> ids)
        {
            return await _context.Set<TEntity>()
                .Where(entity => ids.Contains(entity.Id))
                .ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _context.Set<TEntity>().ToListAsync();
        }

        public async Task AddAsync(TEntity entity)
        {
            entity.RowVersion = new byte[] { 1, 0, 0, 0 };
            await _context.Set<TEntity>().AddAsync(entity);
            await _context.SaveChangesAsync();
        }

        public async Task UpdateAsync(TEntity entity)
        {
            try
            {
                for (int i = entity.RowVersion.Length - 1; i >= 0; i--)
                {
                    if (entity.RowVersion[i] < byte.MaxValue)
                    {
                        entity.RowVersion[i]++;
                        break;
                    }
                    else
                    {
                        entity.RowVersion[i] = 0;
                    }
                }

                _context.Entry(entity).State = EntityState.Modified;
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new InvalidOperationException("Concurrency conflict occurred while updating entity. Please try again.", ex);
            }
            catch (DbUpdateException ex)
            {
                throw new InvalidOperationException("An error occurred while updating entity. Please try again.", ex);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("An unexpected error occurred while updating entity. Please try again.", ex);
            }
        }

        public async Task UpdateRangeAsync(IEnumerable<TEntity> entities)
        {
            try
            {
                foreach (var entity in entities)
                {
                    for (int i = entity.RowVersion.Length - 1; i >= 0; i--)
                    {
                        if (entity.RowVersion[i] < byte.MaxValue)
                        {
                            entity.RowVersion[i]++;
                            break;
                        }
                        else
                        {
                            entity.RowVersion[i] = 0;
                        }
                    }

                    _context.Entry(entity).State = EntityState.Modified;
                }

                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException ex)
            {
                throw new InvalidOperationException("Concurrency conflict occurred while updating entities. Please try again.", ex);
            }
            catch (DbUpdateException ex)
            {
                throw new InvalidOperationException("An error occurred while updating entities. Please try again.", ex);
            }
            catch (Exception ex)
            {
                throw new InvalidOperationException("An unexpected error occurred while updating entities. Please try again.", ex);
            }
        }

        public async Task DeleteAsync(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
            await _context.SaveChangesAsync();
        }

        public IQueryable<TEntity> Query(Expression<Func<TEntity, bool>>? predicate)
        {
            if (predicate == null)
                return _context.Set<TEntity>().AsQueryable();
            return _context.Set<TEntity>().Where(predicate).AsQueryable();
        }
    }
}
