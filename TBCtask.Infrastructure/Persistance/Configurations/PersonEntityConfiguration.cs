﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using TBCtask.Domain.Entities.Person;
using TBCtask.Domain.Enums;

namespace TBCtask.Infrastructure.Persistance.Configurations
{
    public class PersonEntityConfiguration : IEntityTypeConfiguration<PersonEntity>
    {
        public void Configure(EntityTypeBuilder<PersonEntity> builder)
        {
            builder.HasKey(p => p.Id);
            builder.Property(p => p.RowVersion).IsRowVersion();

            builder.HasIndex(p => p.Id).IsUnique();
            builder.HasIndex(p => p.FirstName);
            builder.HasIndex(p => p.LastName);
            builder.HasIndex(p => p.IdentificationNumber);

            builder.Property(p => p.Gender).HasConversion<EnumToStringConverter<GenderType>>();

            builder.HasOne(p => p.City)
                   .WithMany(c => c.People)
                   .HasForeignKey(p => p.CityId)
                   .OnDelete(DeleteBehavior.Restrict);

            builder.HasData(
            new PersonEntity
            {
                Id = 1,
                FirstName = "John",
                LastName = "Doe",
                Gender = GenderType.Male,
                IdentificationNumber = "12345678901",
                DateOfBirth = new DateTime(1980, 1, 15),
                CityId = 1,
                Image = "john_doe.jpg",
                RowVersion = new byte[] { },
                Created = DateTime.UtcNow
            },
            new PersonEntity
            {
                Id = 2,
                FirstName = "Alice",
                LastName = "Smith",
                Gender = GenderType.Female,
                IdentificationNumber = "98765432109",
                DateOfBirth = new DateTime(1985, 5, 20),
                CityId = 2,
                Image = "alice_smith.jpg",
                RowVersion = new byte[] { },
                Created = DateTime.UtcNow
            },
            new PersonEntity
            {
                Id = 3,
                FirstName = "Alice",
                LastName = "Smith",
                Gender = GenderType.Female,
                IdentificationNumber = "98765432109",
                DateOfBirth = new DateTime(1985, 5, 20),
                CityId = 2,
                Image = "alice_smith.jpg",
                RowVersion = new byte[] { },
                Created = DateTime.UtcNow
            }
        );
        }
    }
}
