﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBCtask.Domain.Entities.PhoneNumber;
using TBCtask.Domain.Enums;

namespace TBCtask.Infrastructure.Persistance.Configurations
{
    public class PhoneNumberEntityConfiguration : IEntityTypeConfiguration<PhoneNumberEntity>
    {
        public void Configure(EntityTypeBuilder<PhoneNumberEntity> builder)
        {
            builder.HasKey(p => p.Id);
            builder.HasIndex(p => p.Id).IsUnique();
            builder.HasIndex(p => p.Number);
            builder.Property(p => p.RowVersion).IsRowVersion();

            builder.Property(p => p.NumberType).HasConversion<EnumToStringConverter<PhoneNumberType>>();




            builder.HasOne(c => c.Person)
                .WithMany(p => p.PhoneNumbers)
                .HasForeignKey(p => p.PersonId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasData(
           new PhoneNumberEntity { Id = 1, Number = "1234567890", NumberType = PhoneNumberType.Mobile, PersonId = 1,
               RowVersion = new byte[] { },
               Created = DateTime.UtcNow
           },
           new PhoneNumberEntity { Id = 2, Number = "9876543210", NumberType = PhoneNumberType.Home, PersonId = 2,
               RowVersion = new byte[] { },
               Created = DateTime.UtcNow
           },
           new PhoneNumberEntity { Id = 3, Number = "5555555555", NumberType = PhoneNumberType.Mobile, PersonId = 3,
               RowVersion = new byte[] { },
               Created = DateTime.UtcNow
           }
       );
        }
    }
}
