﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using TBCtask.Domain.Entities.Person;
using TBCtask.Domain.Enums;

namespace TBCtask.Infrastructure.Persistance.Configurations
{
    public class RelatedPersonEntityConfiguration : IEntityTypeConfiguration<RelatedPersonEntity>
    {
        public void Configure(EntityTypeBuilder<RelatedPersonEntity> builder)
        {
            builder.HasKey(p => p.Id);
            builder.HasIndex(p => p.Id).IsUnique();
            builder.HasIndex(rp => rp.PersonId);
            builder.HasIndex(rp => rp.RelatedPersonId);
            builder.HasIndex(rp => rp.RelationshipType);
            builder.Property(p => p.RowVersion).IsRowVersion();

            builder.Property(p => p.RelationshipType).HasConversion<EnumToStringConverter<PersonConnectionType>>();

            builder.HasOne(rp => rp.Person)
                .WithMany(p => p.RelatedPersons)
                .HasForeignKey(rp => rp.PersonId)
                .OnDelete(DeleteBehavior.Restrict);

            builder.HasData(
            new RelatedPersonEntity { Id = 1, PersonId = 1, RelatedPersonId = 2, RelationshipType = PersonConnectionType.Relative,
                RowVersion = new byte[] { },
                Created = DateTime.UtcNow
            },
            new RelatedPersonEntity { Id = 2, PersonId = 2, RelatedPersonId = 3, RelationshipType = PersonConnectionType.Friend,
                RowVersion = new byte[] { },
                Created = DateTime.UtcNow
            }
        );
        }
    }
}
