﻿using Microsoft.EntityFrameworkCore;
using TBCtask.Domain.Entities.City;

namespace TBCtask.Infrastructure.Persistance.Configurations
{
    public class CityEntityConfiguration : IEntityTypeConfiguration<CityEntity>
    {
        public void Configure(Microsoft.EntityFrameworkCore.Metadata.Builders.EntityTypeBuilder<CityEntity> builder)
        {
            builder.HasKey(p => p.Id);
            builder.HasIndex(p => p.Id).IsUnique();
            builder.HasIndex(p => p.Name);
            builder.Property(p => p.RowVersion).IsRowVersion();

            builder.HasData(
            new CityEntity { Id = 1, Name = "New York", RowVersion = new byte[] { }, Created = DateTime.UtcNow },
            new CityEntity { Id = 2, Name = "Los Angeles", RowVersion = new byte[] { }, Created = DateTime.UtcNow },
            new CityEntity { Id = 3, Name = "Chicago", RowVersion = new byte[] { }, Created = DateTime.UtcNow },
            new CityEntity { Id = 4, Name = "Houston", RowVersion = new byte[] { }, Created = DateTime.UtcNow },
            new CityEntity { Id = 5, Name = "Phoenix", RowVersion = new byte[] { }, Created = DateTime.UtcNow },
            new CityEntity { Id = 6, Name = "Philadelphia", RowVersion = new byte[] { }, Created = DateTime.UtcNow },
            new CityEntity { Id = 7, Name = "San Antonio", RowVersion = new byte[] { }, Created = DateTime.UtcNow },
            new CityEntity { Id = 8, Name = "San Diego", RowVersion = new byte[] { }, Created = DateTime.UtcNow },
            new CityEntity { Id = 9, Name = "Dallas", RowVersion = new byte[] { }, Created = DateTime.UtcNow },
            new CityEntity { Id = 10, Name = "San Jose", RowVersion = new byte[] { }, Created = DateTime.UtcNow }
        );
        }
    }
}
