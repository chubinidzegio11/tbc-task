﻿using Microsoft.AspNetCore.Mvc.Filters;
using System.Net;
using System.Web.Http.Controllers;

namespace TBCtask.Infrastructure.ActionFilter
{
    public class RequestValidationFilterAttribute : ActionFilterAttribute, IActionFilter
    {
        // Doesnt use because of fluentValidation
        public void OnActionExecuting(HttpActionContext actionContext)
        {
            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, actionContext.ModelState);
            }
        }
    }
}
