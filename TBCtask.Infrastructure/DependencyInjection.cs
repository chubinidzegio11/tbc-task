﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using TBCtask.Application.Common.Interfaces;
using TBCtask.Domain.Entities.City;
using TBCtask.Domain.Entities.Person;
using TBCtask.Domain.Entities.PhoneNumber;
using TBCtask.Infrastructure.Persistance;
using TBCtask.Infrastructure.Persistance.Repository;
using TBCtask.Infrastructure.Services;

namespace TBCtask.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<AppDbContext>(options =>
                options.UseSqlServer(
                    configuration.GetSection("ConnectionString").Value,
                    b => b.MigrationsAssembly(typeof(AppDbContext).Assembly.FullName)));

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IImageUploadService, ImageUploadService>();

            services.AddScoped<IRepository<PersonEntity>, Repository<PersonEntity>>();
            services.AddScoped<IRepository<PhoneNumberEntity>, Repository<PhoneNumberEntity>>();
            services.AddScoped<IRepository<CityEntity>, Repository<CityEntity>>();
            services.AddScoped<IRepository<RelatedPersonEntity>, Repository<RelatedPersonEntity>>();

            return services;
        }
    }
}
