using AutoMapper;
using FakeItEasy;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Moq;
using TBCtask.Application.Common.Exceptions;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Common.Interfaces;
using TBCtask.Application.Handler.Commands.Person;
using TBCtask.Application.Models.CommandResults.Person;
using TBCtask.Application.Models.Commands.Person;
using TBCtask.Application.Models.Common;
using TBCtask.Domain.Entities.Person;
using Assert = Microsoft.VisualStudio.TestTools.UnitTesting.Assert;

namespace TBCtask.Tests.Application.Handler.Commands.Person
{
    [TestClass]
    public class CreatePersonCommandTests
    {
        [TestMethod]
        public void Handle_WithValidCommand_ShouldReturnSuccessResult()
        {
            // Arrange
            var command = new CreatePersonCommand
            {
                FirstName = "John",
                LastName = "Doe",
                Gender = GenderTypeDto.Male,
                IdentificationNumber = "1234567890",
                DateOfBirth = new DateTime(1990, 01, 01),
                CityId = 1,
                Image = null
            };

            var mappedEntity = new PersonEntity();
            mappedEntity.Id = 1;

            var fakePersonRepo = A.Fake<IRepository<PersonEntity>>();
            var fakeMapper = A.Fake<IMapper>();
            var fakeLogger = A.Fake<ILogger<CreatePersonCommandHandler>>();
            var fakeUploadImageService = A.Fake<IImageUploadService>();

            A.CallTo(() => fakeUploadImageService.UploadAsync(It.IsAny<IFormFile>())).Returns("imagePath");
            A.CallTo(() => fakeMapper.Map<PersonEntity>(command)).Returns(mappedEntity);
            A.CallTo(() => fakePersonRepo.AddAsync(It.IsAny<PersonEntity>())).Returns(Task.CompletedTask);

            var handler = new CreatePersonCommandHandler(fakePersonRepo, fakeMapper, fakeLogger, fakeUploadImageService);

            // Act
            var result = handler.Handle(command, CancellationToken.None).Result;

            // Assert
            Assert.IsInstanceOfType(result, typeof(IResult<CreatePersonCommandResult>));
            Assert.IsTrue(result.IsSucces);
            Assert.IsNotNull(result.Data);
            Assert.AreEqual(1, result.Data.Id);
        }

        [TestMethod]
        public void Handle_WithUploadFailure_ShouldThrowException()
        {
            // Arrange
            var command = new CreatePersonCommand
            {
                FirstName = "John",
                LastName = "Doe",
                Gender = GenderTypeDto.Male,
                IdentificationNumber = "1234567890",
                DateOfBirth = new DateTime(1990, 01, 01),
                CityId = 1,
                Image = null
            };

            var fakePersonRepo = A.Fake<IRepository<PersonEntity>>();
            var fakeMapper = A.Fake<IMapper>();
            var fakeLogger = A.Fake<ILogger<CreatePersonCommandHandler>>();
            var fakeUploadImageService = A.Fake<IImageUploadService>();


            A.CallTo(() => fakeUploadImageService.UploadAsync(It.IsAny<IFormFile>())).Returns(Task.FromResult(string.Empty));

            var handler = new CreatePersonCommandHandler(fakePersonRepo, fakeMapper, fakeLogger, fakeUploadImageService);

            // Act & Assert
            Assert.ThrowsExceptionAsync<DomainException>(() => handler.Handle(command, CancellationToken.None));
        }
    }
}
