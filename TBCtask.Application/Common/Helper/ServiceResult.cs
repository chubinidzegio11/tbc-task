﻿using System.Net;

namespace TBCtask.Application.Common.Helper
{
    public class ServiceResult<TResult> : IResult<TResult>
    {
        public TResult Data { get; set; }
        public string? Message { get; set; }
        public bool IsSucces { get; set; }

        public ServiceResult()
        {
            
        }

        public ServiceResult(TResult data, string? message, bool isSucces)
        {
            Data = data;
            Message = message;
            IsSucces = isSucces;
        }
    }
}
