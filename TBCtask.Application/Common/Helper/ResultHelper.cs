﻿namespace TBCtask.Application.Common.Helper
{
    public static class ResultHelper
    {
        public static IResult<TResult> OK<TResult>(TResult data, string? message = null) where TResult : class
        {
            var result = new ServiceResult<TResult>
            {
                Data = data,
                Message = message,
                IsSucces = true
            };

            return result;
        }

        public static IResult<TResult> Fail<TResult>(string? message, TResult? data = null) where TResult : class
        {
            var result = new ServiceResult<TResult>
            {
                Data = data,
                Message = message,
                IsSucces = false
            };

            return result;
        }
    }
}
