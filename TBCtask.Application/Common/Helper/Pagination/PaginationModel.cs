﻿namespace TBCtask.Application.Common.Helper.Pagination
{
    public class PaginationModel<T> : IPaginationResult<T>
    {
        public List<T>? Data { get; set; }
        public int? TotalCount { get; set; }
        public int? Page { get; set; }
        public int? PageSize { get; set; }

        public PaginationModel()
        {
            
        }

        public PaginationModel(List<T>? items, int? totalCount, int? page, int? pageSize)
        {
            Data = items;
            TotalCount = totalCount;
            Page = page;
            PageSize = pageSize;
        }
    }
}
