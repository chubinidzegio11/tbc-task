﻿namespace TBCtask.Application.Common.Helper.Pagination
{
    public static class PaginationService
    {
        public static IPaginationResult<T> Paginate<T> (IQueryable<T> items, int page, int pageSize) where T : class
        {
            var totalCount = items.Count();
            var pagedItems = items.Skip((page - 1) * pageSize).Take(pageSize).ToList();

            return new PaginationModel<T>(pagedItems, totalCount, page, pageSize)
            {
                Data = pagedItems,
                TotalCount = totalCount,
                Page = page,
                PageSize = pageSize
            };
        }

        public static IPaginationResult<T> PaginationResult<T>(List<T> items, int page, int pageSize, int totalCount) where T : class
        {

            return new PaginationModel<T>(items, totalCount, page, pageSize)
            {
                Data = items,
                TotalCount = totalCount,
                Page = page,
                PageSize = pageSize
            };
        }
    }
}
