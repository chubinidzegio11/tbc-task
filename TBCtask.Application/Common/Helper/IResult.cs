﻿namespace TBCtask.Application.Common.Helper
{
    public interface IResult<TResult>
    {
        TResult Data { get; set; }
        string?  Message { get; set; }
        bool IsSucces { get; set; }
    }
}
