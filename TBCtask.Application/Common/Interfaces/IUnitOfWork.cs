﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TBCtask.Application.Common.Interfaces
{
    public interface IUnitOfWork
    {
        public Task TryCommitAsync(Action action);
        public Task TryCommitAsync(Func<Task> action);
    }
}
