﻿using Microsoft.EntityFrameworkCore;
using TBCtask.Domain.Entities.City;
using TBCtask.Domain.Entities.Person;
using TBCtask.Domain.Entities.PhoneNumber;

namespace TBCtask.Application.Common.Interfaces
{
    public interface IAppDbContext
    {
        DbSet<PersonEntity> Persons { get; }
        DbSet<CityEntity> Cities { get; }
        DbSet<RelatedPersonEntity> RelatedPersons { get; }
        DbSet<PhoneNumberEntity> PhoneNumbers { get; }
    }
}
