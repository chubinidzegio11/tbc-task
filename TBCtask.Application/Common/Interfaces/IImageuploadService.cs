﻿using Microsoft.AspNetCore.Http;

namespace TBCtask.Application.Common.Interfaces
{
    public interface IImageUploadService
    {
        Task<string> UploadAsync(IFormFile file);
    }
}
