﻿using System.Linq.Expressions;
using TBCtask.Domain.Common;
using TBCtask.Domain.Entities.Person;

namespace TBCtask.Application.Common.Interfaces
{
    public interface IRepository<TEntity> where TEntity : BaseEntity
    {
        Task<TEntity> GetByIdAsync(long id);
        Task<IEnumerable<TEntity>> GetAllAsync();
        Task<IEnumerable<TEntity>> GetByIdsAsync(IEnumerable<long> ids);
        Task AddAsync(TEntity entity);
        Task UpdateAsync(TEntity entity);
        Task UpdateRangeAsync(IEnumerable<TEntity> entities);
        Task DeleteAsync(TEntity entity);
        IQueryable<TEntity> Query(Expression<Func<TEntity, bool>>? predicate);
    }
}
