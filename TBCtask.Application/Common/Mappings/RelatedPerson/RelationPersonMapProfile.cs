﻿using AutoMapper;
using TBCtask.Application.Models.Commands.RelatedPerson;
using TBCtask.Domain.Entities.Person;

namespace TBCtask.Application.Common.Mappings.RelatedPerson
{
    public class RelationPersonMapProfile : Profile
    {
        public RelationPersonMapProfile()
        {
            CreateMap<AddRelatedPersonCommand, RelatedPersonEntity>()
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));
        }
    }
}
