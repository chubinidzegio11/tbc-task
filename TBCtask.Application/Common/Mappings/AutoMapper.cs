﻿using Microsoft.Extensions.DependencyInjection;
using TBCtask.Application.Common.Mappings.Person;
using TBCtask.Application.Common.Mappings.RelatedPerson;

namespace TBCtask.Application.Common.Mappings
{
    public static class AutoMapper
    {
        public static void ConfigureAutoMapper(this IServiceCollection services)
        {
            services.AddAutoMapper(typeof(PersonMapProfile));
            services.AddAutoMapper(typeof(RelationPersonMapProfile));
        }
    }
}
