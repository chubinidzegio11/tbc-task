﻿using AutoMapper;
using TBCtask.Application.Models.Commands.Person;
using TBCtask.Application.Models.Common;
using TBCtask.Application.Models.QueryResults.Person;
using TBCtask.Domain.Entities.Person;
using TBCtask.Domain.Entities.PhoneNumber;

namespace TBCtask.Application.Common.Mappings.Person
{
    public class PersonMapProfile : Profile
    {
        public PersonMapProfile()
        {
            CreateMap<CreatePersonCommand, PersonEntity>()
                .ForMember(dest => dest.Image, opt => opt.Ignore())
                .ForMember(dest => dest.RelatedPersons, opt => opt.MapFrom(src => src.RelatedPersons))
                .ForMember(dest => dest.PhoneNumbers, opt => opt.MapFrom(src => src.PhoneNumbers));

            CreateMap<RelatedPersonDto, RelatedPersonEntity>().ReverseMap()
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<PhoneNumberDto, PhoneNumberEntity>().ReverseMap()
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<UpdatePersonCommand, PersonEntity>()
                .ForMember(dest => dest.RelatedPersons, opt => opt.Ignore())
                .ForMember(dest => dest.PhoneNumbers, opt => opt.Ignore())
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<PersonEntity, GetPersonInfoQueryResult>()
                .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<PersonEntity, PersonQuickSeachQueryResult>()
               .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));

            CreateMap<PersonEntity, PersonDetailedSearchQueryResult>()
               .ForAllMembers(opt => opt.Condition((src, dest, srcMember) => srcMember != null));
        }
    }
}
