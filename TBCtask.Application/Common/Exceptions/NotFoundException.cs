﻿using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace TBCtask.Application.Common.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException()
        {
            
        }

        public NotFoundException(string message) : base(message)
        {
            
        }
    }
}
