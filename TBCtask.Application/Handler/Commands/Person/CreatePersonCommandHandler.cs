﻿using AutoMapper;
using MediatR;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBCtask.Application.Common.Exceptions;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Common.Interfaces;
using TBCtask.Application.Models.CommandResults.Person;
using TBCtask.Application.Models.Commands.Person;
using TBCtask.Domain.Entities.Person;

namespace TBCtask.Application.Handler.Commands.Person
{
    public class CreatePersonCommandHandler : IRequestHandler<CreatePersonCommand, IResult<CreatePersonCommandResult>>
    {
        private readonly IRepository<PersonEntity> _personRepo;
        private readonly IMapper _mapper;
        private readonly ILogger<CreatePersonCommandHandler> _logger;
        private readonly IImageUploadService _uploadImageService;


        public CreatePersonCommandHandler(
            IRepository<PersonEntity> personRepo,
            IMapper mapper,
            ILogger<CreatePersonCommandHandler> logger,
            IImageUploadService uploadImageService)
        {
            _personRepo = personRepo;
            _mapper = mapper;
            _logger = logger;
            _uploadImageService = uploadImageService;
        }

        public async Task<IResult<CreatePersonCommandResult>> Handle(CreatePersonCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var imagePath = await _uploadImageService.UploadAsync(command.Image) ?? throw new DomainException("Error uploading image");
                var request = _mapper.Map<PersonEntity>(command);
                request.Image = imagePath;

                await _personRepo.AddAsync(request);

                var result = new CreatePersonCommandResult()
                {
                    Id = request.Id
                };

                return ResultHelper.OK(result);
            }
            catch (Exception)
            {
                _logger.LogError("Error during creating person {0}", command.IdentificationNumber);
                throw;
            }
        }
    }
}
