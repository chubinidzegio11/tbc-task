﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TBCtask.Application.Common.Exceptions;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Common.Interfaces;
using TBCtask.Application.Models.CommandResults.Person;
using TBCtask.Application.Models.Commands.Person;
using TBCtask.Domain.Entities.Person;

namespace TBCtask.Application.Handler.Commands.Person
{
    public class UpdatePersonImageCommandHandler : IRequestHandler<UpdatePersonImageCommand, IResult<UpdatePersonImageCommandResult>>
    {
        private readonly IRepository<PersonEntity> _personRepo;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdatePersonCommandHandler> _logger;
        private readonly IImageUploadService _uploadImageService;

        public UpdatePersonImageCommandHandler(
            IRepository<PersonEntity> personRepo,
            IMapper mapper,
            ILogger<UpdatePersonCommandHandler> logger,
            IImageUploadService uploadImageService)
        {
            _personRepo = personRepo;
            _mapper = mapper;
            _logger = logger;
            _uploadImageService = uploadImageService;
        }

        public async Task<IResult<UpdatePersonImageCommandResult>> Handle(UpdatePersonImageCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var person = await _personRepo
                    .Query(x =>
                        x.Id == command.PersonId &&
                        !x.Deleted)
                    .FirstOrDefaultAsync(cancellationToken) ?? throw new NotFoundException("Person not found");

                var imagePath = await _uploadImageService.UploadAsync(command.File) ?? throw new DomainException("Error uploading image");
                person.Image = imagePath;

                await _personRepo.UpdateAsync(person);

                var result = new UpdatePersonImageCommandResult() { Id = person.Id };

                return ResultHelper.OK(result);
            }
            catch (Exception ex)
            {
                _logger.LogError($"{ex.Message}");
                throw;
            }
        }
    }
}
