﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Common.Interfaces;
using TBCtask.Application.Models.CommandResults.Person;
using TBCtask.Application.Models.Commands.Person;
using TBCtask.Domain.Entities.Person;

namespace TBCtask.Application.Handler.Commands.Person
{
    public class UpdatePersonCommandHandler : IRequestHandler<UpdatePersonCommand, IResult<UpdatePersonCommandResult>>
    {
        private readonly IRepository<PersonEntity> _personRepo;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly ILogger<UpdatePersonCommandHandler> _logger;

        public UpdatePersonCommandHandler(
            IRepository<PersonEntity> personRepo,
            IMapper mapper,
            ILogger<UpdatePersonCommandHandler> logger,
            IUnitOfWork unitOfWork)
        {
            _personRepo = personRepo;
            _mapper = mapper;
            _logger = logger;
            _unitOfWork = unitOfWork;
        }

        public async Task<IResult<UpdatePersonCommandResult>> Handle(UpdatePersonCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var person = await _personRepo.Query(x => x.Id == command.Id).Include(x => x.PhoneNumbers.Where(z => !z.Deleted)).FirstOrDefaultAsync(cancellationToken);
                if (person == null)
                    return ResultHelper.Fail<UpdatePersonCommandResult>("Person not found.");

                _mapper.Map(command, person);

                foreach (var item in command.PhoneNumbers)
                {
                    var destPhoneNumebr = person.PhoneNumbers?.FirstOrDefault(x => x.Id == item.Id);

                    if (destPhoneNumebr != null)
                        _mapper.Map(item, destPhoneNumebr);
                }

                await _unitOfWork.TryCommitAsync(async () =>
                {
                    await _personRepo.UpdateAsync(person);
                });

                var result = new UpdatePersonCommandResult
                {
                    Id = person.Id
                };

                return ResultHelper.OK(result);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error during updating person {0}", command.Id);
                throw;
            }
        }
    }
}
