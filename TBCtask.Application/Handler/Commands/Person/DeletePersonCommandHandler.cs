﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Common.Interfaces;
using TBCtask.Application.Models.CommandResults.Person;
using TBCtask.Application.Models.Commands.Person;
using TBCtask.Domain.Entities.Person;

namespace TBCtask.Application.Handler.Commands.Person
{
    public class DeletePersonCommandHandler : IRequestHandler<DeletePersonCommand, IResult<DeletePersonCommandResult>>
    {
        private readonly IRepository<PersonEntity> _personRepo;
        private readonly ILogger<CreatePersonCommandHandler> _logger;


        public DeletePersonCommandHandler(
            IRepository<PersonEntity> personRepo, 
            ILogger<CreatePersonCommandHandler> logger)
        {
            _personRepo = personRepo;
            _logger = logger;
        }

        public async Task<IResult<DeletePersonCommandResult>> Handle(DeletePersonCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var personEntity = await
                _personRepo
                    .Query(x =>
                        x.Id == command.Id &&
                        !x.Deleted)
                    .Include(x => x.RelatedPersons.Where(z => z.PersonId == command.Id || z.RelatedPersonId == command.Id && !z.Deleted))
                    .Include(x => x.PhoneNumbers.Where(z => !z.Deleted))
                .FirstOrDefaultAsync(cancellationToken);

                if (personEntity == null)
                    return ResultHelper.Fail<DeletePersonCommandResult>("Person doesnt exist");

                personEntity.Deleted = true;

                foreach (var item in personEntity.RelatedPersons)
                {
                    item.Deleted = true;
                }

                foreach (var item in personEntity.PhoneNumbers)
                {
                    item.Deleted = true;
                }

                await _personRepo.UpdateAsync(personEntity);

                var result = new DeletePersonCommandResult() { Id = personEntity.Id };

                return ResultHelper.OK(result);
            }
            catch (Exception ex)
            {
                _logger.LogError("error durin deleting person {0}", command.Id);
                throw;
            }
        }
    }
}
