﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Common.Interfaces;
using TBCtask.Application.Models.CommandResults.RelatedPerson;
using TBCtask.Application.Models.Commands.RelatedPerson;
using TBCtask.Domain.Entities.Person;
using TBCtask.Domain.Enums;

namespace TBCtask.Application.Handler.Commands.RelatedPerson
{
    public class AddRelatedPersonCommadHandler : IRequestHandler<AddRelatedPersonCommand, IResult<AddRelatedPersonCommandResult>>
    {
        private readonly IRepository<RelatedPersonEntity> _relatePersonRepo;
        private readonly IMapper _mapper;
        private readonly ILogger<AddRelatedPersonCommadHandler> _logger;

        public AddRelatedPersonCommadHandler(
            IRepository<RelatedPersonEntity> relatePersonRepo, 
            IMapper mapper, 
            ILogger<AddRelatedPersonCommadHandler> logger)
        {
            _relatePersonRepo = relatePersonRepo;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<IResult<AddRelatedPersonCommandResult>> Handle(AddRelatedPersonCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var addedRelations = new List<long>();

                foreach (var item in command.RelatedPersons)
                {
                    var relationshypType = (PersonConnectionType)item.RelationshipType;

                    var relationExist = await
                        _relatePersonRepo
                            .Query(x =>
                                x.PersonId == item.PersonId &&
                                x.RelatedPersonId == item.RelatedPersonId &&
                                x.RelationshipType == relationshypType &&
                                !x.Deleted)
                        .FirstOrDefaultAsync(cancellationToken);

                    if (relationExist == null)
                    {
                        var entity = _mapper.Map<RelatedPersonEntity>(item);
                        await _relatePersonRepo.AddAsync(entity);
                        addedRelations.Add(entity.Id);
                    }
                }

                if (addedRelations.Count == 0)
                    return ResultHelper.Fail<AddRelatedPersonCommandResult>("Relations alreay exists");

                var result = new AddRelatedPersonCommandResult() { Ids = new List<long>() };

                result.Ids.AddRange(addedRelations);

                return ResultHelper.OK(result);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error during adding relation.");
                throw;
            }
            
        }
    }
}
