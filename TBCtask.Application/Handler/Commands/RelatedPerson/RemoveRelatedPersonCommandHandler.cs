﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Common.Interfaces;
using TBCtask.Application.Models.CommandResults.RelatedPerson;
using TBCtask.Application.Models.Commands.RelatedPerson;
using TBCtask.Domain.Entities.Person;

namespace TBCtask.Application.Handler.Commands.RelatedPerson
{
    public class RemoveRelatedPersonCommandHandler : IRequestHandler<RemoveRelatedPersonCommand, IResult<RemoveRelatedPersonCommandResult>>
    {
        private readonly IRepository<RelatedPersonEntity> _relatePersonRepo;
        private readonly IMapper _mapper;
        private readonly ILogger<AddRelatedPersonCommadHandler> _logger;

        public RemoveRelatedPersonCommandHandler(
            IRepository<RelatedPersonEntity> relatePersonRepo, 
            IMapper mapper,
            ILogger<AddRelatedPersonCommadHandler> logger)
        {
            _relatePersonRepo = relatePersonRepo;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<IResult<RemoveRelatedPersonCommandResult>> Handle(RemoveRelatedPersonCommand command, CancellationToken cancellationToken)
        {
            try
            {
                var removedRelations = new List<long>();

                foreach (var item in command.Ids)
                {
                    var entity = await
                        _relatePersonRepo
                            .Query(x =>
                                x.Id == item &&
                                !x.Deleted)
                        .FirstOrDefaultAsync(cancellationToken);

                    if (entity != null)
                    {
                        // Deleting from database doesnt consider correctly, so using soft delete approach
                        entity.Deleted = true;
                        await _relatePersonRepo.UpdateAsync(entity);
                        removedRelations.Add(entity.Id);
                    }
                }

                if (removedRelations.Count == 0)
                    return ResultHelper.Fail<RemoveRelatedPersonCommandResult>("Relations doesnt exist");

                var result = new RemoveRelatedPersonCommandResult() { Ids = new List<long>() };

                result.Ids.AddRange(removedRelations);

                return ResultHelper.OK(result);
            }
            catch (Exception ex)
            {
                _logger.LogError("Error during adding relation.");
                throw;
            }
        }
    }
}
