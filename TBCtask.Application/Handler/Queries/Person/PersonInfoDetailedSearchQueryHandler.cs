﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TBCtask.Application.Common.Helper.Pagination;
using TBCtask.Application.Common.Interfaces;
using TBCtask.Application.Models.Queries.Person;
using TBCtask.Application.Models.QueryResults.Person;
using TBCtask.Domain.Entities.Person;
using TBCtask.Domain.Enums;

namespace TBCtask.Application.Handler.Queries.Person
{
    public class PersonInfoDetailedSearchQueryHandler : IRequestHandler<PersonDetailedSearchQuery, IPaginationResult<PersonDetailedSearchQueryResult>>
    {
        private readonly IRepository<PersonEntity> _personRepo;
        private readonly IMapper _mapper;
        private readonly ILogger<GetPersonInfoQueryQueryHandler> _logger;

        public PersonInfoDetailedSearchQueryHandler(
            IRepository<PersonEntity> personRepo, 
            IMapper mapper,
            ILogger<GetPersonInfoQueryQueryHandler> logger)
        {
            _personRepo = personRepo;
            _mapper = mapper;
            _logger = logger;
        }
        public async Task<IPaginationResult<PersonDetailedSearchQueryResult>> Handle(PersonDetailedSearchQuery query, CancellationToken cancellationToken)
        {
            GenderType? genderType = null;
            PersonConnectionType? relationshipType = null;
            PhoneNumberType? phoneNumberType = null;

            var person = _personRepo.Query(x => !x.Deleted)
                .Include(x => x.RelatedPersons)
                .Include(x => x.PhoneNumbers)
                .AsQueryable();

            if (query.FirstName != null)
                person = person.Where(x => x.FirstName == query.FirstName);
            if (query.LastName != null)
                person = person.Where(x => x.LastName == query.LastName);
            if (query.Gender != null)
            {
                genderType = (GenderType)query.Gender;
                person = person.Where(x => x.Gender == genderType);
            }
            if (query.IdentificationNumber != null)
                person = person.Where(x => x.IdentificationNumber == query.IdentificationNumber);
            if (query.DateOfBirth != null)
                person = person.Where(x => x.DateOfBirth == query.DateOfBirth);
            if (query.CityId != null)
                person = person.Where(x => x.CityId == query.CityId);

            if (query.RelatedPerson != null)
            {
                if (query.RelatedPerson.PersonId != null)
                    person = person.Where(x => x.RelatedPersons.Select(z => z.PersonId == query.RelatedPerson.PersonId).FirstOrDefault());
                if (query.RelatedPerson.RelatedPersonId != null)
                    person = person.Where(x => x.RelatedPersons.Select(z => z.RelatedPersonId == query.RelatedPerson.RelatedPersonId).FirstOrDefault());
                if (query.RelatedPerson.RelationshipType != null)
                {
                    relationshipType = (PersonConnectionType)query.RelatedPerson.RelationshipType;
                    person = person.Where(x => x.RelatedPersons.Select(z => z.RelationshipType == relationshipType).FirstOrDefault());
                }
            }

            if (query.PhoneNumber != null)
            {
                if (query.PhoneNumber.Number != null)
                    person = person.Include(x => x.PhoneNumbers).Where(x => x.PhoneNumbers.Select(z => z.Number == query.PhoneNumber.Number).FirstOrDefault());

                if (query.PhoneNumber.NumberType != null)
                {
                    phoneNumberType = (PhoneNumberType)query.PhoneNumber.NumberType;
                    person = person.Include(x => x.PhoneNumbers).Where(x => x.PhoneNumbers.Select(z => z.NumberType == phoneNumberType).FirstOrDefault());
                }
            }

            var paginationResult = PaginationService.Paginate(person, query.Page, query.PageSize);

            var personsInfo = _mapper.Map<List<PersonDetailedSearchQueryResult>>(paginationResult.Data);

            return PaginationService.PaginationResult(personsInfo, paginationResult.Page.Value, paginationResult.PageSize.Value, paginationResult.TotalCount.Value);

        }
    }
}
