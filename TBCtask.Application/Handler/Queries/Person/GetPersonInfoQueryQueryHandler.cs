﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TBCtask.Application.Common.Exceptions;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Common.Interfaces;
using TBCtask.Application.Models.Queries.Person;
using TBCtask.Application.Models.QueryResults.Person;
using TBCtask.Domain.Entities.Person;

namespace TBCtask.Application.Handler.Queries.Person
{
    public class GetPersonInfoQueryQueryHandler : IRequestHandler<GetPersonInfoQuery, IResult<GetPersonInfoQueryResult>>
    {
        private readonly IRepository<PersonEntity> _personRepo;
        private readonly IMapper _mapper;
        private readonly ILogger<GetPersonInfoQueryQueryHandler> _logger;

        public GetPersonInfoQueryQueryHandler(
            IRepository<PersonEntity> personRepo,
            IMapper mapper, 
            ILogger<GetPersonInfoQueryQueryHandler> logger)
        {
            _personRepo = personRepo;
            _mapper = mapper;
            _logger = logger;
        }
        public async Task<IResult<GetPersonInfoQueryResult>> Handle(GetPersonInfoQuery query, CancellationToken cancellationToken)
        {
            var personEntity = await _personRepo
                .Query(x =>
                    x.Id == query.Id &&
                    !x.Deleted)
                .Include(x => x.RelatedPersons.Where(z => !z.Deleted))
                .Include(x => x.PhoneNumbers).Where(z => !z.Deleted)
            .FirstOrDefaultAsync(cancellationToken);

            if (personEntity == null)
                throw new NotFoundException("Person not found");

            var result = _mapper.Map<GetPersonInfoQueryResult>(personEntity);

            return ResultHelper.OK(result);
        }
    }
}
