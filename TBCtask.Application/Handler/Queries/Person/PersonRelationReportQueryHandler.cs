﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Common.Interfaces;
using TBCtask.Application.Models.Queries.Person;
using TBCtask.Application.Models.QueryResults.Person;
using TBCtask.Domain.Entities.Person;

namespace TBCtask.Application.Handler.Queries.Person
{
    public class PersonRelationReportQueryHandler : IRequestHandler<PersonRelationsReportQuery, IResult<List<PersonRelationReportQueryResult>>>
    {
        private readonly IRepository<RelatedPersonEntity> _relatedtPersonRepo;
        private readonly IMapper _mapper;
        private readonly ILogger<PersonRelationReportQueryHandler> _logger;

        public PersonRelationReportQueryHandler(
            IRepository<RelatedPersonEntity> relatedtPersonRepo, 
            IMapper mapper, 
            ILogger<PersonRelationReportQueryHandler> logger)
        {
            _relatedtPersonRepo = relatedtPersonRepo;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<IResult<List<PersonRelationReportQueryResult>>> Handle(PersonRelationsReportQuery request, CancellationToken cancellationToken)
        {
            try
            {
                var personReport = await _relatedtPersonRepo
               .Query(x =>
                   !x.Deleted)
               .GroupBy(x => new { x.PersonId, x.RelationshipType })
               .Select(x => new PersonRelationReportQueryResult
               {
                   PersonId = x.Key.PersonId,
                   RelationshipType = x.Key.RelationshipType,
                   Count = x.Count()
               })
               .ToListAsync(cancellationToken);

                return ResultHelper.OK(personReport);
            }
            catch (Exception)
            {
                _logger.LogError("Error during geting person relation report");
                throw;
            }
           
        }
    }
}
