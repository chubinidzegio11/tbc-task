﻿using AutoMapper;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using TBCtask.Application.Common.Helper.Pagination;
using TBCtask.Application.Common.Interfaces;
using TBCtask.Application.Models.Queries.Person;
using TBCtask.Application.Models.QueryResults.Person;
using TBCtask.Domain.Entities.Person;

namespace TBCtask.Application.Handler.Queries.Person
{
    public class PersonInfoQuickSearchCommandHandler : IRequestHandler<PersonQuickSearchQuery, IPaginationResult<PersonQuickSeachQueryResult>>
    {
        private readonly IRepository<PersonEntity> _personRepo;
        private readonly IMapper _mapper;
        private readonly ILogger<GetPersonInfoQueryQueryHandler> _logger;

        public PersonInfoQuickSearchCommandHandler(
            IRepository<PersonEntity> personRepo, 
            IMapper mapper, 
            ILogger<GetPersonInfoQueryQueryHandler> logger)
        {
            _personRepo = personRepo;
            _mapper = mapper;
            _logger = logger;
        }

        public async Task<IPaginationResult<PersonQuickSeachQueryResult>> Handle(PersonQuickSearchQuery query, CancellationToken cancellationToken)
        {
            try
            {
                var personEntities = _personRepo
                .Query(x =>
                     EF.Functions.Like(x.FirstName, $"%{query.SearchTerm}%") ||
                     EF.Functions.Like(x.LastName, $"%{query.SearchTerm}%") ||
                     EF.Functions.Like(x.IdentificationNumber, $"%{query.SearchTerm}%"))
                .Include(x => x.RelatedPersons).Where(z => !z.Deleted)
                .Include(x => x.PhoneNumbers).Where(z => !z.Deleted);

                var paginationResult = PaginationService.Paginate(personEntities, query.Page, query.PageSize);

                var personsInfo = _mapper.Map<List<PersonQuickSeachQueryResult>>(paginationResult.Data);

                return PaginationService.PaginationResult(personsInfo, paginationResult.Page.Value, paginationResult.PageSize.Value, paginationResult.TotalCount.Value);
            }
            catch (Exception)
            {
                _logger.LogError("Error during quick search.");
                throw;
            }
        }
    }
}
