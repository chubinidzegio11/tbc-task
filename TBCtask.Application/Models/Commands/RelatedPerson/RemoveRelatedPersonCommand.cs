﻿using FluentValidation;
using MediatR;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Models.CommandResults.RelatedPerson;
using TBCtask.Application.Models.Common;

namespace TBCtask.Application.Models.Commands.RelatedPerson
{
    public class RemoveRelatedPersonCommand : IRequest<IResult<RemoveRelatedPersonCommandResult>>
    {
        public List<long> Ids { get; set; }
    }

    public class RemoveRelatedPersonCommandValidator : AbstractValidator<RemoveRelatedPersonCommand>
    {
        public RemoveRelatedPersonCommandValidator()
        {
            RuleForEach(x => x.Ids)
                .ChildRules(x =>
                {
                    x.RuleFor(x => x).GreaterThan(0).WithMessage("Id must be greater than 0");
                });
        }
    }
}
