﻿using FluentValidation;
using MediatR;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Models.CommandResults.RelatedPerson;
using TBCtask.Application.Models.Common;

namespace TBCtask.Application.Models.Commands.RelatedPerson
{
    public class AddRelatedPersonCommand : IRequest<IResult<AddRelatedPersonCommandResult>>
    {
        public List<RelatedPersonDto> RelatedPersons { get; set; }
    }

    public class AddRelatedPersonCommandValidator : AbstractValidator<AddRelatedPersonCommand>
    {
        public AddRelatedPersonCommandValidator()
        {
            RuleForEach(x => x.RelatedPersons)
                .ChildRules(relatedPerson =>
                {
                    relatedPerson.RuleFor(x => x.RelatedPersonId)
                       .GreaterThan(0).WithMessage("Related person ID must be greater than 0.")
                       .NotEqual(x => x.PersonId).WithMessage("RelatedPersonId cannot be the same as PersonId.");

                    relatedPerson.RuleFor(x => x.RelationshipType)
                        .NotNull().WithMessage("Relationship type is required.");

                    relatedPerson.RuleFor(x => x.PersonId)
                        .GreaterThan(0).WithMessage("Person ID must be greater than 0.");
                });
        }
    }
}
