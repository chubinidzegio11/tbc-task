﻿using FluentValidation;
using MediatR;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Models.CommandResults.Person;

namespace TBCtask.Application.Models.Commands.Person
{
    public class DeletePersonCommand : IRequest<IResult<DeletePersonCommandResult>>
    {
        public long Id { get; set; }
    }

    public class DeletePersonCommandValidator : AbstractValidator<DeletePersonCommand>
    {
        public DeletePersonCommandValidator()
        {
            RuleFor(x => x.Id)
                .GreaterThan(0)
                .WithMessage("Id must be greater than 0");
        }
    }
}
