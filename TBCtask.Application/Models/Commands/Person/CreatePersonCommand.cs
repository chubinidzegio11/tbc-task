﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Models.CommandResults.Person;
using TBCtask.Application.Models.Common;

namespace TBCtask.Application.Models.Commands.Person
{
    public class CreatePersonCommand : IRequest<IResult<CreatePersonCommandResult>>
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public GenderTypeDto Gender { get; set; }
        public string IdentificationNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public IFormFile Image { get; set; }
        public long CityId { get; set; }
        public ICollection<RelatedPersonDto> RelatedPersons { get; set; } = new List<RelatedPersonDto>();
        public ICollection<PhoneNumberDto> PhoneNumbers { get; set; } = new List<PhoneNumberDto>();
    }

    public class CreatePersonCommandValidator : AbstractValidator<CreatePersonCommand>
    {
        public CreatePersonCommandValidator()
        {
            RuleFor(x => x.FirstName)
                .NotEmpty().WithMessage("First name is required.")
                .Length(2, 50).WithMessage("First name length must be between 2 and 50 characters.");

            RuleFor(x => x.LastName)
                .NotEmpty().WithMessage("Last name is required.")
                .Length(2, 50).WithMessage("Last name length must be between 2 and 50 characters.");

            RuleFor(x => x.IdentificationNumber)
                .NotEmpty().WithMessage("Identification number is required.")
                .Length(11).WithMessage("Identification number must be 11 characters.");

            RuleFor(x => x.CityId)
                .NotEmpty().WithMessage("City is required.")
                .GreaterThan(0).WithMessage("City id must be greater than 0.");

            RuleFor(x => x.DateOfBirth)
                .NotEmpty().WithMessage("Date of birth is required.")
                .Must(Be18YearsOrOlder).WithMessage("Person must be at least 18 years old.");

            RuleForEach(x => x.RelatedPersons)
                .ChildRules(relatedPerson =>
                {
                    relatedPerson.RuleFor(x => x.RelatedPersonId)
                        .GreaterThan(0).WithMessage("Related person ID must be greater than 0.")
                        .NotEqual(x => x.PersonId).WithMessage("RelatedPersonId cannot be the same as PersonId.");

                    relatedPerson.RuleFor(x => x.RelationshipType)
                        .IsInEnum().WithMessage("Invalid relationship type.");
                });

            RuleForEach(x => x.PhoneNumbers)
                .ChildRules(phoneNumber =>
                {
                    phoneNumber.RuleFor(x => x.NumberType)
                        .IsInEnum().WithMessage("Invalid phone number type.");

                    phoneNumber.RuleFor(x => x.Number)
                        .NotEmpty().WithMessage("Phone number is required.")
                        .MinimumLength(4).WithMessage("Phone number must be at least 4 characters.")
                        .MaximumLength(50).WithMessage("Phone number cannot exceed 50 characters.");
                });
        }

        private bool Be18YearsOrOlder(DateTime dateOfBirth)
        {
            return dateOfBirth <= DateTime.Today.AddYears(-18);
        }
    }
}
