﻿using FluentValidation;
using MediatR;
using Microsoft.AspNetCore.Http;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Models.CommandResults.Person;

namespace TBCtask.Application.Models.Commands.Person
{
    public class UpdatePersonImageCommand : IRequest<IResult<UpdatePersonImageCommandResult>>
    {
        public long PersonId { get; set; }
        public IFormFile File { get; set; }
    }

    public class UpdatePersonImageCommandValidator : AbstractValidator<UpdatePersonImageCommand>
    {
        public UpdatePersonImageCommandValidator()
        {
            RuleFor(x => x.PersonId)
              .GreaterThan(0).WithMessage("Id must be greater than 0.");

            RuleFor(x => x.File)
                .NotEmpty()
                .WithMessage("File must be provided");
        }
    }
}
