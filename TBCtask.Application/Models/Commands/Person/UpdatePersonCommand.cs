﻿using FluentValidation;
using MediatR;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Models.CommandResults.Person;
using TBCtask.Application.Models.Common;

namespace TBCtask.Application.Models.Commands.Person
{
    public class UpdatePersonCommand : IRequest<IResult<UpdatePersonCommandResult>>
    {
        public long Id { get; set; }

        public string? FirstName { get; set; }

        public string? LastName { get; set; }

        public GenderTypeDto? Gender { get; set; }

        public string? IdentificationNumber { get; set; }

        public DateTime? DateOfBirth { get; set; }

        public long? CityId { get; set; }

        public ICollection<PhoneNumberDto> PhoneNumbers { get; set; } = new List<PhoneNumberDto>();
    }  
    

    public class UpdatePersonInfoCommandValidator : AbstractValidator<UpdatePersonCommand>
    {
        public UpdatePersonInfoCommandValidator()
        {
            RuleFor(x => x.Id)
               .GreaterThan(0).WithMessage("Id must be greater than 0.");

            RuleFor(x => x.FirstName)
                .Length(2, 50).WithMessage("First name length must be between 2 and 50 characters.");

            RuleFor(x => x.LastName)
                .Length(2, 50).WithMessage("Last name length must be between 2 and 50 characters.");

            RuleFor(x => x.IdentificationNumber)
                .Length(11).WithMessage("Identification number must be 11 characters.");

            RuleFor(x => x.DateOfBirth)
                .Must(Be18YearsOrOlder).WithMessage("Person must be at least 18 years old.");

            RuleForEach(x => x.PhoneNumbers)
                .ChildRules(phoneNumber =>
                {
                    phoneNumber.RuleFor(x => x.Id)
                       .GreaterThan(0).WithMessage("Id must be greater than 0.");

                    phoneNumber.RuleFor(x => x.NumberType)
                        .IsInEnum().WithMessage("Invalid phone number type.");

                    phoneNumber.RuleFor(x => x.Number)
                        .NotEmpty().WithMessage("Phone number is required.")
                        .MinimumLength(4).WithMessage("Phone number must be at least 4 characters.")
                        .MaximumLength(50).WithMessage("Phone number cannot exceed 50 characters.");
                });
        }

        private bool Be18YearsOrOlder(DateTime? dateOfBirth)
        {
            if(dateOfBirth.HasValue)
                return dateOfBirth <= DateTime.Today.AddYears(-18);

            return true;
        }
    }
}
