﻿using System.Collections;

namespace TBCtask.Application.Models.CommandResults.RelatedPerson
{
    public class AddRelatedPersonCommandResult
    {
        public List<long> Ids { get; set; }
    }
}
