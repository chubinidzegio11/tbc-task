﻿namespace TBCtask.Application.Models.CommandResults.RelatedPerson
{
    public class RemoveRelatedPersonCommandResult
    {
        public List<long> Ids { get; set; }
    }
}
