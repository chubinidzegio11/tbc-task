﻿namespace TBCtask.Application.Models.CommandResults.Person
{
    public class DeletePersonCommandResult
    {
        public long Id { get; set; }
    }
}
