﻿using TBCtask.Domain.Enums;

namespace TBCtask.Application.Models.QueryResults.Person
{
    public class PersonRelationReportQueryResult
    {
        public long PersonId { get; set; }
        public PersonConnectionType RelationshipType{ get; set; }
        public int Count { get; set; }
    }
}
