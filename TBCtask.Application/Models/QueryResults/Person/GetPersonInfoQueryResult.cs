﻿using TBCtask.Application.Models.Common;
using TBCtask.Domain.Enums;

namespace TBCtask.Application.Models.QueryResults.Person
{
    public class GetPersonInfoQueryResult
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public GenderTypeDto Gender { get; set; }
        public string IdentificationNumber { get; set; }
        public DateTime DateOfBirth { get; set; }
        public string? Image { get; set; }
        public long CityId { get; set; }
        public ICollection<RelatedPersonDto>? RelatedPersons { get; set; } = new List<RelatedPersonDto>();
        public ICollection<PhoneNumberDto>? PhoneNumbers { get; set; } = new List<PhoneNumberDto>();
    }
}
