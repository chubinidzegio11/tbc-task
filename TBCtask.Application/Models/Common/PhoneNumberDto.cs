﻿namespace TBCtask.Application.Models.Common
{
    public class PhoneNumberDto
    {
        public long? Id { get; set; }
        public PhoneNumberTypeDto? NumberType { get; set; }
        public string? Number { get; set; }
    }
}
