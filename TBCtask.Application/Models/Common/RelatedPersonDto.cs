﻿namespace TBCtask.Application.Models.Common
{
    public class RelatedPersonDto
    {
        public long? PersonId { get; set; }
        public long? RelatedPersonId { get; set; }
        public PersonConnectionTypeDto? RelationshipType { get; set; }
    }
}
