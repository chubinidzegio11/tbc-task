﻿namespace TBCtask.Application.Models.Common
{
    public enum PhoneNumberTypeDto
    {
        Mobile = 1,
        Home = 2,
        Office = 3
    }
}
