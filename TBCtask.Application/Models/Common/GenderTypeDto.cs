﻿namespace TBCtask.Application.Models.Common
{
    public enum GenderTypeDto
    {
        Male = 1,
        Female = 2,
    }
}
