﻿namespace TBCtask.Application.Models.Common
{
    public enum PersonConnectionTypeDto
    {
        Relative,
        Friend,
        Colleague,
        Acquaintance,
        Other
    }
}
