﻿using FluentValidation;
using MediatR;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Models.QueryResults.Person;

namespace TBCtask.Application.Models.Queries.Person
{
    public class GetPersonInfoQuery : IRequest<IResult<GetPersonInfoQueryResult>>
    {
        public long Id { get; set; }
    }

    public class GetPersonInfoValidator : AbstractValidator<GetPersonInfoQuery>
    {
        public GetPersonInfoValidator()
        {
            RuleFor(x => x.Id)
                .NotEmpty().WithMessage("Id must be provided.");
        }
    }
}
