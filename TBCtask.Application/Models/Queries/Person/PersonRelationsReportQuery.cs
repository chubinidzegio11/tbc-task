﻿using MediatR;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Models.QueryResults.Person;

namespace TBCtask.Application.Models.Queries.Person
{
    public class PersonRelationsReportQuery : IRequest<IResult<List<PersonRelationReportQueryResult>>>
    {
        
    }
}
