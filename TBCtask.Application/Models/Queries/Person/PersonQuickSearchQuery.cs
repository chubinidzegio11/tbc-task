﻿using FluentValidation;
using MediatR;
using TBCtask.Application.Common.Helper.Pagination;
using TBCtask.Application.Models.QueryResults.Person;

namespace TBCtask.Application.Models.Queries.Person
{
    public class PersonQuickSearchQuery : IRequest<IPaginationResult<PersonQuickSeachQueryResult>>
    {
        public string SearchTerm { get; set; }
        public int Page { get; set; }
        public int PageSize { get; set; }
    }

    public class PersonQuickSearchhValidator : AbstractValidator<PersonQuickSearchQuery>
    {
        public PersonQuickSearchhValidator()
        {
            RuleFor(x => x.Page)
                .NotEmpty().WithMessage("Page must be provided.");

            RuleFor(x => x.PageSize)
                .NotEmpty().WithMessage("Page size must be provided.");

            RuleFor(x => x.SearchTerm)
                .NotEmpty().NotNull().WithMessage("Search term must be provided.");
        }
    }
}
