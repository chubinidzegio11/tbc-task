﻿using FluentValidation;
using MediatR;
using TBCtask.Application.Common.Helper;
using TBCtask.Application.Common.Helper.Pagination;
using TBCtask.Application.Models.Commands.Person;
using TBCtask.Application.Models.Common;
using TBCtask.Application.Models.QueryResults.Person;

namespace TBCtask.Application.Models.Queries.Person
{
    public  class PersonDetailedSearchQuery : IRequest<IPaginationResult<PersonDetailedSearchQueryResult>>
    {
        public string? FirstName { get; set; }
        public string? LastName { get; set; }
        public GenderTypeDto? Gender { get; set; }
        public string? IdentificationNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public long? CityId { get; set; }
        public RelatedPersonDto? RelatedPerson { get; set; } = new RelatedPersonDto();
        public PhoneNumberDto? PhoneNumber { get; set; } = new PhoneNumberDto();

        public int Page { get; set; }
        public int PageSize { get; set; }
    }

    public class PersonDetailedSearchValidator : AbstractValidator<PersonDetailedSearchQuery>
    {
        public PersonDetailedSearchValidator()
        {
            RuleFor(x => x.Page)
                .GreaterThan(0).WithMessage("Page must be provided.");

            RuleFor(x => x.PageSize)
                .GreaterThan(0).WithMessage("Page size must be provided.");
        }
    }
}
