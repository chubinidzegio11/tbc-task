﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TBCtask.Application.Common.Helper.Pagination;
using TBCtask.Application.Common.Mappings;

namespace TBCtask.Application
{
    public static class DependencyInjection
    {
        public static object AddApplication(this IServiceCollection services)
        {
            services.ConfigureAutoMapper();

            return services;
        }
    }
}
