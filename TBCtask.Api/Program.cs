using FluentValidation.AspNetCore;
using Microsoft.AspNetCore.Localization;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using System.Globalization;
using System.Reflection;
using TBCtask.Application;
using TBCtask.Application.Common.Mappings;
using TBCtask.Application.Models.Commands.Person;
using TBCtask.Infrastructure;
using TBCtask.Infrastructure.ActionFilter;
using TBCtask.Infrastructure.Middleware;
using TBCtask.Infrastructure.Persistance;

var builder = WebApplication.CreateBuilder(args);
// Add services to the container.
builder.Services.AddInfrastructure(builder.Configuration);
builder.Services.AddApplication();
builder.Services.ConfigureAutoMapper();

builder.Services.AddControllers(options =>
{

}).AddFluentValidation(config => config.RegisterValidatorsFromAssembly(typeof(CreatePersonCommand).GetTypeInfo().Assembly));

builder.Services.AddLocalization(options => options.ResourcesPath = "Resources");

// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddMediatR(cfg => cfg.RegisterServicesFromAssemblies(AppDomain.CurrentDomain.GetAssemblies()));

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

using (var scope = app.Services.CreateScope())
{
    var db = scope.ServiceProvider.GetRequiredService<AppDbContext>();

    db.Database.Migrate();
}

app.UseHttpsRedirection();

app.UseMiddleware<LocalizationMiddleware>(new string[] { "en-US", "ka-GE" },"en-US" );

app.UseRouting();

app.UseAuthorization();

app.UseMiddleware<ErrorHandlingMiddleware>();

app.MapControllers();

app.Run();
