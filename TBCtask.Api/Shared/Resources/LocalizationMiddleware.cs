﻿using FluentValidation;
using System.Globalization;

public class LocalizationMiddleware
{
    private readonly RequestDelegate _next;
    private readonly string[] _supportedCultures;
    private readonly string _defaultCulture;

    public LocalizationMiddleware(RequestDelegate next, string[] supportedCultures, string defaultCulture)
    {
        _next = next;
        _supportedCultures = supportedCultures;
        _defaultCulture = defaultCulture;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        var cultureName = ExtractCultureFromHeader(context.Request.Headers);

        if (!_supportedCultures.Contains(cultureName))
        {
            cultureName = _defaultCulture;
        }

        CultureInfo.CurrentCulture = CultureInfo.CreateSpecificCulture(cultureName);
        ValidatorOptions.Global.LanguageManager.Culture = CultureInfo.CurrentCulture;

        await _next(context);
    }

    private string ExtractCultureFromHeader(IHeaderDictionary headers)
    {
        var preferredCultures = headers["Accept-Language"].ToString().Split(',').Select(x => x.Trim()).ToList();
        
        foreach (var culture in preferredCultures)
        {
            if (CultureInfo.GetCultureInfo(culture) != null)
            {
                return culture;
            }
        }

        foreach (var culture in preferredCultures)
        {
            var baseCulture = new CultureInfo(culture.Split('-')[0]);
            if (CultureInfo.GetCultureInfo(baseCulture.Name) != null)
            {
                return baseCulture.Name;
            }
        }

        return _defaultCulture;
    }
}