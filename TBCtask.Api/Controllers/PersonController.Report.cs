﻿using Microsoft.AspNetCore.Mvc;
using TBCtask.Application.Models.Queries.Person;

namespace TBCtask.Api.Controllers
{
    public partial class PersonController
    {
        [HttpGet("report")]
        public async Task<ActionResult> Retrieve([FromQuery] PersonRelationsReportQuery query, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(query, cancellationToken);
            return Ok(result);
        }
    }
}
