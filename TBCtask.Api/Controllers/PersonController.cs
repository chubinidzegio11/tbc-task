﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Identity.Client;
using TBCtask.Application.Models.Commands.Person;
using TBCtask.Application.Models.Queries.Person;
using TBCtask.Infrastructure.ActionFilter;

namespace TBCtask.Api.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public partial class PersonController : ControllerBase
    {
        private readonly IMediator _mediator;

        public PersonController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult> Create([FromForm] CreatePersonCommand command, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }

        [HttpPut]
        public async Task<ActionResult> Update([FromBody] UpdatePersonCommand command, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }

        [HttpPut("update/image")]
        public async Task<ActionResult> UpdateImage([FromForm] UpdatePersonImageCommand command,  CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }

        [HttpDelete]
        public async Task<ActionResult> Delete([FromBody] DeletePersonCommand command, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }


        [HttpGet]
        public async Task<ActionResult> Query([FromQuery] GetPersonInfoQuery query, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(query, cancellationToken);
            return Ok(result);
        }
    }
}
