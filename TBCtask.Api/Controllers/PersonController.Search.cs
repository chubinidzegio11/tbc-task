﻿using Microsoft.AspNetCore.Mvc;
using TBCtask.Application.Models.Queries.Person;

namespace TBCtask.Api.Controllers
{
    public partial class PersonController
    {
        [HttpGet("search/quick")]
        public async Task<ActionResult> QuickSearch([FromQuery] PersonQuickSearchQuery query, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(query, cancellationToken);
            return Ok(result);
        }

        [HttpGet("search/detailed")]
        public async Task<ActionResult> DetailedSearch([FromQuery] PersonDetailedSearchQuery query, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(query, cancellationToken);
            return Ok(result);
        }
    }
}
