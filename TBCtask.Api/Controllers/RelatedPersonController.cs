﻿using MediatR;
using Microsoft.AspNetCore.Mvc;
using TBCtask.Application.Models.Commands.RelatedPerson;

namespace TBCtask.Api.Controllers
{
    [ApiController]
    [Route("v1/[controller]")]
    public class RelatedPersonController : ControllerBase
    {
        private readonly IMediator _mediator;

        public RelatedPersonController(IMediator mediator)
        {
            _mediator = mediator;
        }

        [HttpPost]
        public async Task<ActionResult> Add([FromBody]AddRelatedPersonCommand command, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }

        [HttpDelete]
        public async Task<ActionResult> Remove([FromBody] RemoveRelatedPersonCommand command, CancellationToken cancellationToken)
        {
            var result = await _mediator.Send(command, cancellationToken);
            return Ok(result);
        }
    }
}
